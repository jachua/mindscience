# **MindSpore SPONGE暑期学校新手教程**

## **Cybertron**

### **获取Cybertron dataset**

tutorial_c00.py所需数据集可从以下网址下载：

dataset_qm9.npz: http://gofile.me/6Utp7/tJ5hoDIAo

ethanol_dft.npz: http://gofile.me/6Utp7/hbQBofAFM

### **获取Cybertron checkpoint**

```bash
cd mindscience/MindSPONGE/tutorials/cybertron/
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/Cybertron/checkpoint_c10.ckpt
```

## **GAN**

### **获取GAN checkpoint以及result**

GAN所需数据集可以从[此处](http://yann.lecun.com/exdb/mnist/)下载

### **获取GAN checkpoint以及result**

```bash
cd mindscience/MindSPONGE/tutorials/GAN/ckpts/opt_ckpts/
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/GAN/generator_epoch_50000.ckpt
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/GAN/model_epoch_50000.ckpt
```

## **IDM**

### **获取IDM checkpoint**

```bash
cd mindscience/MindSPONGE/tutorials/IDM/ckpts/resolutions/
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/IDM/resolutions_4_model_epoch_5000.ckpt
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/IDM/resolutions_50_model_epoch_5000.ckpt
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/IDM/resolutions_200_model_epoch_5000.ckpt
wget https://download.mindspore.cn/mindscience/mindsponge/ckpts/IDM/resolutions_5000_model_epoch_5000.ckpt
```