[ENGLISH](README_EN.md) | 简体中文

# **MindSPONGE-APPLICATIONS**

- [简介](#简介)
- [目录](#目录)

## **简介**

Application底层依托计算生物工具包MindSPONGE以及昇思MindSpore构建。旨在为大家提供丰富的计算生物案例，同时也欢迎大家为MindSPONGE提供更多更优秀的案例。

## **目录**

- [分子动力学](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/molecular_dynamics/)
    - [蛋白质松弛](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/molecular_dynamics/protein_relaxation)
    - [传统分子动力学](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/molecular_dynamics/tradition)
- [MEGA-Protein](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/MEGAProtein/)
    - [MEGA-Fold](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/MEGAProtein/model/fold.py)
    - [to be updated]()
