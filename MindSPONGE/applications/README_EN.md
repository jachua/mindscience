ENGLISH | [简体中文](README.md)

# **MindSPONGE-APPLICATIONS**

- [**MindSPONGE-APPLICATIONS**](#mindsponge-applications)
    - [**Introduction**](#introduction)
    - [**Contents**](#contents)

## **Introduction**

Application is based on MindSPONGE and MindSpore. We aim to provide powerful applications. Any suggestion is welcome.

## **Contents**

- [Molecular Dynamics](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/molecular_dynamics/)
    - [Protein Relaxation](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/molecular_dynamics/protein_relaxation)
    - [Traditional MD](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/molecular_dynamics/tradition)
- [MEGA-Protein](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/MEGAProtein/)
    - [MEGA-Fold](https://gitee.com/mindspore/mindscience/tree/master/MindSPONGE/applications/MEGAProtein/model/fold.py)
    - [to be released]()
